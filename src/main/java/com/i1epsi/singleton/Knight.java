/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.i1epsi.singleton;

import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author TVall
 */
public class Knight {
    private static Knight instance;
    
    private AtomicInteger health = new AtomicInteger (1);
    
    private Knight(){
        //singleton
    }
    
    public static Knight getInstance(){
        if (instance == null){
            instance = new Knight();
        }
        return instance;
    }
}
