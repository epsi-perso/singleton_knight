/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.i1epsi.singleton;

/**
 *
 * @author TVall
 */
public class Warfield {
    public static void main(String[] args) {
        for (int i = 0; i < 3; i++){
            Thread t = new Thread(new Monster(), "Monster" + i);
            t.start();
        }
    }
}
